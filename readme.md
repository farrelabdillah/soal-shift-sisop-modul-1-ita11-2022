# README

Soal Shift Modul 1

Sistem Operasi 2022

1. Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

a. Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script [main.sh](http://main.sh/)  

b. Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
i. Minimal 8 karakter
ii. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
iii. Alphanumeric
iv. Tidak boleh sama dengan username

c. Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss **MESSAGE**. Message pada log akan berbeda tergantung aksi yang dilakukan user.
i. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
ii. Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User **USERNAME** registered successfully
iii. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user **USERNAME**
iv. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User **USERNAME** logged in

d. Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :

i. dl N ( N = Jumlah gambar yang akan didownload)
Untuk mendownload gambar dari [https://loremflickr.com/320/240](https://loremflickr.com/320/240) dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama **YYYY-MM-DD**_**USERNAME**. Gambar-gambar yang didownload juga memiliki format nama **PIC_XX**, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.     

ii. att
Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.



2. Pada tanggal 22 Januari 2022, website [https://daffa.info](https://daffa.info/) di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website [https://daffa.info](https://daffa.info/) Buatlah sebuah script awk bernama **"soal2_forensic_dapos.sh"** untuk melaksanakan tugas-tugas berikut:

a. Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.

b. Dikarenakan serangan yang diluncurkan ke website [https://daffa.info](https://daffa.info/) sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.

c. Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website [https://daffa.inf](https://daffa.info/)o, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.

d. Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.

e. Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.

Agar Dapos tidak bingung saat membaca hasilnya, formatnya akan dibuat seperti ini:

- File ratarata.txt

Rata-rata serangan adalah sebanyak rata_rata requests per jam

- File result.txt

IP yang paling banyak mengakses server adalah: ip_address sebanyak jumlah_request requests

Ada jumlah_req_curl requests yang menggunakan curl sebagai user-agent

IP Address Jam 2 pagi

IP Address Jam 2 pagi

dst

- Gunakan AWK
- Nanti semua file-file HASIL SAJA yang akan dimasukkan ke dalam folder forensic_log_website_daffainfo_log

3. Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.
Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.
    
a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
    
c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
    
d.Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.
    

Note:

- nama file untuk script per menit adalah minute_log.sh
- nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
- semua file log terletak di /home/{user}/log

Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:

mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size

15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M

Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:

type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size

minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M

maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M

average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62

JAWABAN

1.

Proses pengerjaan saya dimulai dari [register.sh](http://register.sh) kemudian ke main.sh

Untuk soal nomor 1A diperlukan agar register dapat menyimpan data user yang mendaftar di file /users/user.txt. Disini kita siapkan dulu variabel password, folder, dan juga users. Kita buat directory baru kemudian gunakan grep untuk mendapatkan informasi registrasi yang berada di user.txt.

```bash
Password=${#password}
folder=/home/zawoon/MODUL1/shift1
Users=$folder/users
  
    if [[ ! -d "$Users" ]]
    then
	mkdir $Users
    fi
    
    if grep -q -w "$username" "$Users/user.txt"```

```

Untuk soal nomor 1B terdapat beberapa requirement untuk password. Input password perlu tertutup sehingga kita taruh di -s di echo terakhir untuk input password :

```bash
echo "Enter username: "
read username

echo "Enter password: "
read -s password
```

Kemudian, password harus memiliki minimal 8 karakter, 1 upper, 1 lower, alphanum, dan juga tidak boleh sama dengan user. Jika tidak maka akan proses akan berhenti. Command yang digunakan adalah :

```bash
    elif [[ $Password -lt 8  || "$password" != *[A-Z]* || "$password" != *[a-z]* || "$password" != *[0-9]* || $password == $username ]]
    then 
    	echo "Password must meet requirements!"
    else
```

c. Karena kita harus membuat log untuk percobaan register dan login, masukan informasi ke log.txt. Format yang diperlukan adalah MM/DD/YY hh:mm:ss. Disini kita akan menformat timestamp menggunakan :

```bash
calendar=$(date +%D)
time=$(date +%T)
```

Untuk 1CI, percobaan register yang gagal akan dioutputkan di log.txt dengan line REGISTER: ERROR User already exists. Disini gunakan command :

```bash
    if grep -q -w "$username" "$Users/user.txt"
    then
        echo $calendar $time REGISTER:ERROR User already exists >> $folder/log.txt
    	echo "User already exists"
```

Untuk 1CII, percobaan register yang berhasil akan dioutputkan juga di log.txt dimana line REGISTER: INFO User **USERNAME**
 registered successfull akan masuk. Gunakan command :

```bash
      echo $calendar $time REGISTER:INFO User $username registered successfully >> $folder/log.txt
    	echo $username $password >> $Users/user.txt
    	echo "Register successfull!"
```

Untuk function terakhir, pastikan ketika proses registrasi dilakukan terminal digunakan oleh root user :

```bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
	exit 1
else
	func_login
fi
```

Selanjutnya untuk function main.sh, sama seperti register.sh, timestamp harus diformat untuk .txt. Kemudian, directory harus didefinisikan. Oleh karena itu gunakan :

```bash
calendar=$(date +%D)
time=$(date +%T)

folder=$(date +%Y-%m-%d)_$username
log=/home/zawoon/MODUL1/shift1/log.txt
user=/home/zawoon/MODUL1/shift1/users/user.txt
```

Selanjutnya, dibuat fungsi func_login untuk pengecekan akun dan att. Pertama buat fungsi if menggunakan grep yang akan mengecek apakah user sudah diregister, apakah password benar, dan sesuai dengan soal 1CIV juga menginputkan timestamp login ke log.txt jika berhasil login :

```bash
func_login(){
	if [[ ! -f "$user" ]]
	then
		echo "No user registered"
	else
	if grep -q -w "$username" "$user"
		then
	if grep -q -w "$username $password" "$user"
		then
		echo "$calendar $time LOGIN:INFO User $username logged in" >> $log
		echo "You're logged in"
```

Perlu diingat juga pada nomor 1CIII bahwa percobaan login attempt yang gagal harus dicatat log.txt. Untuk command line ini user akan dinotifikasi jika gagal melakukan login meskipun username benar (akan dioutputkan ke log.txt) dan juga gagal melakukan login karena belum melakukan registrasi(tidak dioutputkan ke log.txt) :

```bash
	else
	fail="Failed login attemp on user $username"
		echo $fail

		echo "$calendar $time LOGIN:ERROR $fail" >> $log
			fi
		else
			echo "User not found, please register!"
		fi
	fi
}
```

Jika login berhasil dilakukan, maka sesuai dengan perintah soal 1DII, user bisa melihat jumlah percobaan login dari log.txt :

```bash
	echo "Enter command : "
	read command
	if [[ $command == att ]]
	then
		func_att
	else
		echo "Command not found"
	fi

```

Dari funct_att, akan diarahkan ke fungsi yang mengecek apakah terdapat log dari login, jika log tidak ditemukan maka proses akan berhenti, jika proses ditemukan maka akan dieksekusi fungsi awk yang mencari login attempt dari user :

```bash
func_awk(){
  awk -v user="$username" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count-1)}' $log
}

func_att(){
    if [[ ! -f "$log" ]]
    then
        echo "Log not found"
    else
        func_awk
    fi
}
```

Mengikuti soal nomor 1B terdapat beberapa requirement untuk password. Input password perlu tertutup sehingga kita taruh di -s di echo terakhir untuk input password :

```bash
echo "Enter your username: "
read username

echo "Enter your password: "
read -s password
```

Terakhir, pastikan ketika proses login terjadi, login harus dilakukan terminal digunakan oleh root user :

```bash
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
	exit 1
else
	func_login
fi
```

ERROR LIST :

Tidak bisa membaca .txt dan folder yang dibuat dari gui, harus menggunakan mkdir

Error EOF

Format log untuk waktu tidak benar

Format log untuk waktu tidak keregister

User yang sama masih bisa register

2. 

Untuk soal nomor 2 ini, kita diminta untuk “membantu” dapos membaca log berisi request berbahaya dari website miliknya yang telah di hack dengan menggunakan script awk bernama “soal2_forensic_dapos.sh”.

pertama-tama, sebelum melakukan analisa kita akan menetapkan command untuk melakukan pencarian file / locating file. Sebagai contoh, untuk code ini kami menggunakan:

***search=/home/farrel/soal2
folder=$search/forensic_log_website_daffainfo_log
logfile=$search/log_website_daffainfo.log***

1. Untuk menyimpan hasil analisa dari log, kita akan membuat folder “bernama forensic_log_website_daffainfo_log”. dengan menggunakan command *mkdir* untuk membuat directory.
    
    ***mkdir <nama directory>***
    
2. Banyaknya serangan yang dilakukan kepada website membuat dapos ingin mengetahui rata-rata request menuju website per jam. 
    
    mulai dari sini hingga seterusnya, kami menggunakan *cat.* yaitu kependekan dari *concatenate* yang digunakan untuk membaca data 
    
    dari file dan menghasilkan output sesuai dengan isi file tersebut. 
    
    ***cat $logfile***
    
    Line diatas bermaksud untuk membaca file yang terdapat di dalam log (***logfile=$search/log_website_daffainfo.log***) sehingga sintax awk bisa dijalankan. 
    
    Terdapat sebuah command untuk memempermudah analisa dengan melakukan substitusi yaitu ***gsub.*** pengaplikasiannya dalam code ini digunakan untuk memodifikasi array agar bisa melakukan perhitungan sesuai dengan perintah yang diberikan.
    
    Untuk mencari rata-rata per jam dalam file log, akan dihitung total request yang dilakukan kepada website dengan melakukan pencarian string “Request”.
    
    ***for (i in arr) {
       if (i != "Request"){
       n+=arr[i]
       }
    }***
    
    Setelah ditemukan berapa banyak request yang dilakukan, hasil akan dibagi 12 karena total jangka waktu dalam log tersebut adalah 12 jam. Kemudian, hasil yang didapatkan akan dimasukan kedalam file bernama ratarata.txt dengan menggunakan:
    
    ***>> $folder/ratarata.txt***
    
    Rata-rata serangan= 81,083333
    
3. Selanjutnya, akan ditampilkan IP yang melakukan penyerangan paling banyak. Untuk ini kita dapat menggunakan algoritma untuk mencari berapa kali angka yang sama muncul. Sama seperti soal b, pada bagian ini kami juga menggunakan ***cat*** dan ***gsub.*** 
    
    ***{gsub(/"/, "", $1)
    arr[$1]++}
    END {
           ip
           total=0
          for (i in arr){
          if (total < arr[i]){
               ip = i
               total = arr[ip]
              }***
    
    ***}***
    
    hasil yang didapatkan adalah IP 216.93.144.47 sebanyak 539 request.
    
4. dalam pencarian user-agent curl, untuk mencarinya cukup simpel yaitu hanya dengan menggunakan pencarian string secara spesifik dengan ***/”/***. sebagai contoh dalam code:
    
    ***/curl/ {c++}***
    
    line diatas bertujuan untuk mencari string curl yang terdapat pada log lalu menghitungnya satu persatu agar bisa dijumlahkan keseluruhannya
    
    total request yang menggunakan user-agent curl: 58
    
5. pada soal terakhir ini kami diminta untuk mencari IP yang melakukan serangan pada jam 2 pagi lalu dimasukan kedalam daftar dengan menggunakan:
    
    ***{if($3=="02") printf "%s Jam 2 Pagi \n", $1}***
    
    line diatas menggunakan input if untuk mencari parameter ketiga($3) pada log, yaitu waktu. Setelah ditemukan waktu yang memiliki angka 02 (yang berarti jam 2) di depan, akan langsung dimasukkan kedalam daftar lalu akan dioutputkan parameter pertamanya ($1) yaitu IP.
    

ERROR YANG DIALAMI:

- pencarian directory masih sering salah sehingga selalu direvisi hingga akhirnya berjalan.
- kesulitan mengambil nilai pada parameter karena sebelumnya tidak mengetahui gsub
- hasil rata-rata tidak sesuai karena belum menyadari bahwa total jangka waktu log dalam satu hari (tanggal 22) tersebut adalah 12 jam
- hasil list untuk penyerangan pada jam 2 tidak terdisplay dengan baik dikarenakan parameter tidak sesuai
- kebingungan mencari algoritma untuk mendapatkan IP yang melakukan penyerangan terbanyak sehingga hasil data yang didapat juga tidak sesuai

3.  

Pada soal ini kita diminta untuk memonitoring resource pada komputer berupa
RAM dan size pada suatu directory. Dengan menggunakan command 'free -m' untuk
RAM dan 'du sh </home/noxclow>' untuk directory. Disini noxclow merupakan nama user.

A.

Pertama kita akan membuat script bernama 'minute_log.sh' script ini akan
memonitoring resource pada komputer setiap menitnya lalu akan disimpan kedalam
log sesuai dengan format nama yaitu metrics_{YmdHms}.log.

Pada script 'minute_log.sh' kita akan buat penamaan file log terlebih dahulu
dengan menambahkan

```bash
dateTime="metrics_"$(date +"%Y%m%d%H%M%S")
```

B.

Lalu kita akan membuat script untuk memonitoring RAM dan Direcrory size, untuk memonitoring RAM kita menggunakan command free -m dan untuk directory size kita menggunakan du -sh

Agar kita bisa mengambil data hanya sesuai yang kita mau kita akan menggunakan awk sebagai tambahan dari command yang akan kita buat

Untuk awk pada command RAM kita tidak mengambil data seperti mem usaed, mem free, swap shared, dll. Oleh karena itu dengan menggunakan awk kita akan memprint semua baris dari free -m dimulai dari baris kedua dan kolom kedua, ini berarti kita tidak memprint baris pertama dan kolom pertama.

```bash
ramUsage=$(free -m | awk 'BEGIN{} {for (i=2; i<=NF; i++) {if (NR != 1) printf("%s,", $i);}}' >> /home/noxclow/log/$dateTime.log)
directorySize=$(du -sh /home/noxclow | awk '{printf $2 ", " $1}' >> /home/noxclow/log/$dateTime.log)
```

D.

Disini kita akan memodifikasi akses dari file log dimana hanya owner dari file tersebutlah
yang mendapatkan hak full akses untuk melihat dan mengedit file tersebut. Kita juga perlu
mengatur file secara manual dengan pengaturan permission di linux sehingga untuk akses grup dan other
menjadi none dan akan mengunci foldernya untuk user selain owner file.
Kita akan menambahkan pada script.

```bash
chmod 400 /home/noxclow/log/"metrics_$dateTime.log" 
```

LIST ERROR :

1. Nama file yang sesuai format tidak muncul atau tidak sesuai.
2. Hasil dari cronjob tidak masuk kedalam file log.
